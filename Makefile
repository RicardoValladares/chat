
push:
	git status
	git add .
	git commit -m "$$(date)"
	git pull origin main 
	git push origin main

dependens:
	# pacman -S android-sdk
	# pacman -S android-sdk-platform-tools
	apt-get install -y android-sdk 
	apt-get install -y google-android-ndk-installer
	go install fyne.io/fyne/v2/cmd/fyne@latest

compile:
	go build main.go
	#export ANDROID_NDK_HOME="/usr/lib/android-ndk/" && export ANDROID_HOME="/usr/lib/android-sdk/" && export PATH="${PATH}:${ANDROID_HOME}tools/:${ANDROID_HOME}platform-tools/" && ~/go/bin/fyne package -os android -appID gitlab.com.ricardovalladares.chat
	fyne package -os android -appID gitlab.com.ricardovalladares.chat
	