package chat

import (
	"strings"
	"io/ioutil"
	"net/http"
	"net/url"
	"bytes"
	 "time"
	 "net"
)

const urlchat = "http://ravr.webcindario.com/5_chat/consola.php"

type Sala struct {
    saladechat []byte    
}

func (Chat *Sala) Interaccion(usuario, mensaje string) ([][]string, bool) {
	linkparseado, conexionerror := url.Parse(urlchat)
	if conexionerror != nil {
		return nil, false
	}
	parametros := url.Values{}
	parametros.Add("nombre", usuario)
	parametros.Add("mensaje", mensaje)
	linkparseado.RawQuery = parametros.Encode()
	transport := http.Transport{
			Dial: dialTimeout,
	}
	client := http.Client{
		Transport: &transport,
	}
	respuesta, conexionerror := client.Get(linkparseado.String()) 
	if conexionerror != nil {
		return nil, false
	}
	defer respuesta.Body.Close()
	if respuesta.StatusCode != 200 {
		return nil, false
	}
	saladechat, conexionerror := ioutil.ReadAll(respuesta.Body)
	if conexionerror != nil {
		return nil, false
	}
	respuesta.Body.Close()
	if len(saladechat) == 0 {
		return nil, false
	}
	if len(Chat.saladechat) == 0 {
		Chat.saladechat = saladechat
		return filtrar(strings.Count(string(saladechat),"\n"), string(saladechat)), true
	}
	if bytes.Equal(saladechat, Chat.saladechat) {
		return nil, false
	}
	mensajes := strings.Split(string(Chat.saladechat) , "\n")
	ultimomsj := mensajes[len(mensajes)-1]
	lineas_nuevas := strings.Split(string(saladechat), ultimomsj)
	Chat.saladechat = saladechat
	return filtrar(strings.Count(lineas_nuevas[len(lineas_nuevas)-1],"\n"), lineas_nuevas[len(lineas_nuevas)-1]), true
}

func filtrar(NLineas int, Lineas string) [][]string {
	lineas := strings.Split(Lineas , "\n")
	matrix := make([][]string, NLineas)
	for i:=0; i<=NLineas; i++ {
		campos := strings.Split(lineas[i], ";")
		if len(campos)==3 {
			matrix = append(matrix, []string{ campos[1]+": ", campos[2] } )					
		}
	}
	return matrix
}

func dialTimeout(network, addr string) (net.Conn, error) {
    return net.DialTimeout(network, addr, time.Duration(1 * time.Second))
}