package colores

import (
	"image/color"
)
 
var (
	Aliceblue            = color.RGBA{0xf0, 0xf8, 0xff, 0xff} 
	Antiquewhite         = color.RGBA{0xfa, 0xeb, 0xd7, 0xff} 
	Aqua                 = color.RGBA{0x00, 0xff, 0xff, 0xff} 
	Aquamarine           = color.RGBA{0x7f, 0xff, 0xd4, 0xff} 
	Azure                = color.RGBA{0xf0, 0xff, 0xff, 0xff} 
	Beige                = color.RGBA{0xf5, 0xf5, 0xdc, 0xff} 
	Bisque               = color.RGBA{0xff, 0xe4, 0xc4, 0xff} 
	Black                = color.RGBA{0x00, 0x00, 0x00, 0xff} 
	Blanchedalmond       = color.RGBA{0xff, 0xeb, 0xcd, 0xff} 
	Blue                 = color.RGBA{0x00, 0x00, 0xff, 0xff} 
	Blueviolet           = color.RGBA{0x8a, 0x2b, 0xe2, 0xff} 
	Brown                = color.RGBA{0xa5, 0x2a, 0x2a, 0xff} 
	Burlywood            = color.RGBA{0xde, 0xb8, 0x87, 0xff} 
	Cadetblue            = color.RGBA{0x5f, 0x9e, 0xa0, 0xff} 
	Chartreuse           = color.RGBA{0x7f, 0xff, 0x00, 0xff} 
	Chocolate            = color.RGBA{0xd2, 0x69, 0x1e, 0xff} 
	Coral                = color.RGBA{0xff, 0x7f, 0x50, 0xff} 
	Cornflowerblue       = color.RGBA{0x64, 0x95, 0xed, 0xff} 
	Cornsilk             = color.RGBA{0xff, 0xf8, 0xdc, 0xff} 
	Crimson              = color.RGBA{0xdc, 0x14, 0x3c, 0xff} 
	Cyan                 = color.RGBA{0x00, 0xff, 0xff, 0xff} 
	Darkblue             = color.RGBA{0x00, 0x00, 0x8b, 0xff} 
	Darkcyan             = color.RGBA{0x00, 0x8b, 0x8b, 0xff} 
	Darkgoldenrod        = color.RGBA{0xb8, 0x86, 0x0b, 0xff} 
	Darkgray             = color.RGBA{0xa9, 0xa9, 0xa9, 0xff} 
	Darkgreen            = color.RGBA{0x00, 0x64, 0x00, 0xff} 
	Darkgrey             = color.RGBA{0xa9, 0xa9, 0xa9, 0xff} 
	Darkkhaki            = color.RGBA{0xbd, 0xb7, 0x6b, 0xff} 
	Darkmagenta          = color.RGBA{0x8b, 0x00, 0x8b, 0xff} 
	Darkolivegreen       = color.RGBA{0x55, 0x6b, 0x2f, 0xff} 
	Darkorange           = color.RGBA{0xff, 0x8c, 0x00, 0xff} 
	Darkorchid           = color.RGBA{0x99, 0x32, 0xcc, 0xff} 
	Darkred              = color.RGBA{0x8b, 0x00, 0x00, 0xff} 
	Darksalmon           = color.RGBA{0xe9, 0x96, 0x7a, 0xff} 
	Darkseagreen         = color.RGBA{0x8f, 0xbc, 0x8f, 0xff} 
	Darkslateblue        = color.RGBA{0x48, 0x3d, 0x8b, 0xff} 
	Darkslategray        = color.RGBA{0x2f, 0x4f, 0x4f, 0xff} 
	Darkslategrey        = color.RGBA{0x2f, 0x4f, 0x4f, 0xff} 
	Darkturquoise        = color.RGBA{0x00, 0xce, 0xd1, 0xff} 
	Darkviolet           = color.RGBA{0x94, 0x00, 0xd3, 0xff} 
	Deeppink             = color.RGBA{0xff, 0x14, 0x93, 0xff} 
	Deepskyblue          = color.RGBA{0x00, 0xbf, 0xff, 0xff} 
	Dimgray              = color.RGBA{0x69, 0x69, 0x69, 0xff} 
	Dimgrey              = color.RGBA{0x69, 0x69, 0x69, 0xff} 
	Dodgerblue           = color.RGBA{0x1e, 0x90, 0xff, 0xff} 
	Firebrick            = color.RGBA{0xb2, 0x22, 0x22, 0xff} 
	Floralwhite          = color.RGBA{0xff, 0xfa, 0xf0, 0xff} 
	Forestgreen          = color.RGBA{0x22, 0x8b, 0x22, 0xff} 
	Fuchsia              = color.RGBA{0xff, 0x00, 0xff, 0xff} 
	Gainsboro            = color.RGBA{0xdc, 0xdc, 0xdc, 0xff} 
	Ghostwhite           = color.RGBA{0xf8, 0xf8, 0xff, 0xff} 
	Gold                 = color.RGBA{0xff, 0xd7, 0x00, 0xff} 
	Goldenrod            = color.RGBA{0xda, 0xa5, 0x20, 0xff} 
	Gray                 = color.RGBA{0x80, 0x80, 0x80, 0xff} 
	Green                = color.RGBA{0x00, 0x80, 0x00, 0xff} 
	Greenyellow          = color.RGBA{0xad, 0xff, 0x2f, 0xff} 
	Grey                 = color.RGBA{0x80, 0x80, 0x80, 0xff} 
	Honeydew             = color.RGBA{0xf0, 0xff, 0xf0, 0xff} 
	Hotpink              = color.RGBA{0xff, 0x69, 0xb4, 0xff} 
	Indianred            = color.RGBA{0xcd, 0x5c, 0x5c, 0xff} 
	Indigo               = color.RGBA{0x4b, 0x00, 0x82, 0xff} 
	Ivory                = color.RGBA{0xff, 0xff, 0xf0, 0xff} 
	Khaki                = color.RGBA{0xf0, 0xe6, 0x8c, 0xff} 
	Lavender             = color.RGBA{0xe6, 0xe6, 0xfa, 0xff} 
	Lavenderblush        = color.RGBA{0xff, 0xf0, 0xf5, 0xff} 
	Lawngreen            = color.RGBA{0x7c, 0xfc, 0x00, 0xff} 
	Lemonchiffon         = color.RGBA{0xff, 0xfa, 0xcd, 0xff} 
	Lightblue            = color.RGBA{0xad, 0xd8, 0xe6, 0xff} 
	Lightcoral           = color.RGBA{0xf0, 0x80, 0x80, 0xff} 
	Lightcyan            = color.RGBA{0xe0, 0xff, 0xff, 0xff} 
	Lightgoldenrodyellow = color.RGBA{0xfa, 0xfa, 0xd2, 0xff} 
	Lightgray            = color.RGBA{0xd3, 0xd3, 0xd3, 0xff} 
	Lightgreen           = color.RGBA{0x90, 0xee, 0x90, 0xff} 
	Lightgrey            = color.RGBA{0xd3, 0xd3, 0xd3, 0xff} 
	Lightpink            = color.RGBA{0xff, 0xb6, 0xc1, 0xff} 
	Lightsalmon          = color.RGBA{0xff, 0xa0, 0x7a, 0xff} 
	Lightseagreen        = color.RGBA{0x20, 0xb2, 0xaa, 0xff} 
	Lightskyblue         = color.RGBA{0x87, 0xce, 0xfa, 0xff} 
	Lightslategray       = color.RGBA{0x77, 0x88, 0x99, 0xff} 
	Lightslategrey       = color.RGBA{0x77, 0x88, 0x99, 0xff} 
	Lightsteelblue       = color.RGBA{0xb0, 0xc4, 0xde, 0xff} 
	Lightyellow          = color.RGBA{0xff, 0xff, 0xe0, 0xff} 
	Lime                 = color.RGBA{0x00, 0xff, 0x00, 0xff} 
	Limegreen            = color.RGBA{0x32, 0xcd, 0x32, 0xff} 
	Linen                = color.RGBA{0xfa, 0xf0, 0xe6, 0xff} 
	Magenta              = color.RGBA{0xff, 0x00, 0xff, 0xff} 
	Maroon               = color.RGBA{0x80, 0x00, 0x00, 0xff} 
	Mediumaquamarine     = color.RGBA{0x66, 0xcd, 0xaa, 0xff} 
	Mediumblue           = color.RGBA{0x00, 0x00, 0xcd, 0xff} 
	Mediumorchid         = color.RGBA{0xba, 0x55, 0xd3, 0xff} 
	Mediumpurple         = color.RGBA{0x93, 0x70, 0xdb, 0xff} 
	Mediumseagreen       = color.RGBA{0x3c, 0xb3, 0x71, 0xff} 
	Mediumslateblue      = color.RGBA{0x7b, 0x68, 0xee, 0xff} 
	Mediumspringgreen    = color.RGBA{0x00, 0xfa, 0x9a, 0xff} 
	Mediumturquoise      = color.RGBA{0x48, 0xd1, 0xcc, 0xff} 
	Mediumvioletred      = color.RGBA{0xc7, 0x15, 0x85, 0xff} 
	Midnightblue         = color.RGBA{0x19, 0x19, 0x70, 0xff} 
	Mintcream            = color.RGBA{0xf5, 0xff, 0xfa, 0xff} 
	Mistyrose            = color.RGBA{0xff, 0xe4, 0xe1, 0xff} 
	Moccasin             = color.RGBA{0xff, 0xe4, 0xb5, 0xff} 
	Navajowhite          = color.RGBA{0xff, 0xde, 0xad, 0xff} 
	Navy                 = color.RGBA{0x00, 0x00, 0x80, 0xff} 
	Oldlace              = color.RGBA{0xfd, 0xf5, 0xe6, 0xff} 
	Olive                = color.RGBA{0x80, 0x80, 0x00, 0xff} 
	Olivedrab            = color.RGBA{0x6b, 0x8e, 0x23, 0xff} 
	Orange               = color.RGBA{0xff, 0xa5, 0x00, 0xff} 
	Orangered            = color.RGBA{0xff, 0x45, 0x00, 0xff} 
	Orchid               = color.RGBA{0xda, 0x70, 0xd6, 0xff} 
	Palegoldenrod        = color.RGBA{0xee, 0xe8, 0xaa, 0xff} 
	Palegreen            = color.RGBA{0x98, 0xfb, 0x98, 0xff} 
	Paleturquoise        = color.RGBA{0xaf, 0xee, 0xee, 0xff} 
	Palevioletred        = color.RGBA{0xdb, 0x70, 0x93, 0xff} 
	Papayawhip           = color.RGBA{0xff, 0xef, 0xd5, 0xff} 
	Peachpuff            = color.RGBA{0xff, 0xda, 0xb9, 0xff} 
	Peru                 = color.RGBA{0xcd, 0x85, 0x3f, 0xff} 
	Pink                 = color.RGBA{0xff, 0xc0, 0xcb, 0xff} 
	Plum                 = color.RGBA{0xdd, 0xa0, 0xdd, 0xff} 
	Powderblue           = color.RGBA{0xb0, 0xe0, 0xe6, 0xff} 
	Purple               = color.RGBA{0x80, 0x00, 0x80, 0xff} 
	Red                  = color.RGBA{0xff, 0x00, 0x00, 0xff} 
	Rosybrown            = color.RGBA{0xbc, 0x8f, 0x8f, 0xff} 
	Royalblue            = color.RGBA{0x41, 0x69, 0xe1, 0xff} 
	Saddlebrown          = color.RGBA{0x8b, 0x45, 0x13, 0xff} 
	Salmon               = color.RGBA{0xfa, 0x80, 0x72, 0xff} 
	Sandybrown           = color.RGBA{0xf4, 0xa4, 0x60, 0xff} 
	Seagreen             = color.RGBA{0x2e, 0x8b, 0x57, 0xff} 
	Seashell             = color.RGBA{0xff, 0xf5, 0xee, 0xff} 
	Sienna               = color.RGBA{0xa0, 0x52, 0x2d, 0xff} 
	Silver               = color.RGBA{0xc0, 0xc0, 0xc0, 0xff} 
	Skyblue              = color.RGBA{0x87, 0xce, 0xeb, 0xff} 
	Slateblue            = color.RGBA{0x6a, 0x5a, 0xcd, 0xff} 
	Slategray            = color.RGBA{0x70, 0x80, 0x90, 0xff} 
	Slategrey            = color.RGBA{0x70, 0x80, 0x90, 0xff} 
	Snow                 = color.RGBA{0xff, 0xfa, 0xfa, 0xff} 
	Springgreen          = color.RGBA{0x00, 0xff, 0x7f, 0xff} 
	Steelblue            = color.RGBA{0x46, 0x82, 0xb4, 0xff} 
	Tan                  = color.RGBA{0xd2, 0xb4, 0x8c, 0xff} 
	Teal                 = color.RGBA{0x00, 0x80, 0x80, 0xff} 
	Thistle              = color.RGBA{0xd8, 0xbf, 0xd8, 0xff} 
	Tomato               = color.RGBA{0xff, 0x63, 0x47, 0xff} 
	Turquoise            = color.RGBA{0x40, 0xe0, 0xd0, 0xff} 
	Violet               = color.RGBA{0xee, 0x82, 0xee, 0xff} 
	Wheat                = color.RGBA{0xf5, 0xde, 0xb3, 0xff} 
	White                = color.RGBA{0xff, 0xff, 0xff, 0xff} 
	Whitesmoke           = color.RGBA{0xf5, 0xf5, 0xf5, 0xff} 
	Yellow               = color.RGBA{0xff, 0xff, 0x00, 0xff} 
	Yellowgreen          = color.RGBA{0x9a, 0xcd, 0x32, 0xff} 
)

var (
	Aliceblue_hex            = "#f0f8ff" 
	Antiquewhite_hex         = "#faebd7" 
	Aqua_hex                 = "#00ffff" 
	Aquamarine_hex           = "#7fffd4" 
	Azure_hex                = "#f0ffff" 
	Beige_hex                = "#f5f5dc" 
	Bisque_hex               = "#ffe4c4" 
	Black_hex                = "#000000" 
	Blanchedalmond_hex       = "#ffebcd" 
	Blue_hex                 = "#0000ff" 
	Blueviolet_hex           = "#8a2be2" 
	Brown_hex                = "#a52a2a" 
	Burlywood_hex            = "#deb887" 
	Cadetblue_hex            = "#5f9ea0" 
	Chartreuse_hex           = "#7fff00" 
	Chocolate_hex            = "#d2691e" 
	Coral_hex                = "#ff7f50" 
	Cornflowerblue_hex       = "#6495ed" 
	Cornsilk_hex             = "#fff8dc" 
	Crimson_hex              = "#dc143c" 
	Cyan_hex                 = "#00ffff" 
	Darkblue_hex             = "#00008b" 
	Darkcyan_hex             = "#008b8b" 
	Darkgoldenrod_hex        = "#b8860b" 
	Darkgray_hex             = "#a9a9a9" 
	Darkgreen_hex            = "#006400" 
	Darkgrey_hex             = "#a9a9a9" 
	Darkkhaki_hex            = "#bdb76b" 
	Darkmagenta_hex          = "#8b008b" 
	Darkolivegreen_hex       = "#556b2f" 
	Darkorange_hex           = "#ff8c00" 
	Darkorchid_hex           = "#9932cc" 
	Darkred_hex              = "#8b0000" 
	Darksalmon_hex           = "#e9967a" 
	Darkseagreen_hex         = "#8fbc8f" 
	Darkslateblue_hex        = "#483d8b" 
	Darkslategray_hex        = "#2f4f4f" 
	Darkslategrey_hex        = "#2f4f4f" 
	Darkturquoise_hex        = "#00ced1" 
	Darkviolet_hex           = "#9400d3" 
	Deeppink_hex             = "#ff1493" 
	Deepskyblue_hex          = "#00bfff" 
	Dimgray_hex              = "#696969" 
	Dimgrey_hex              = "#696969" 
	Dodgerblue_hex           = "#1e90ff" 
	Firebrick_hex            = "#b22222" 
	Floralwhite_hex          = "#fffaf0" 
	Forestgreen_hex          = "#228b22" 
	Fuchsia_hex              = "#ff00ff" 
	Gainsboro_hex            = "#dcdcdc" 
	Ghostwhite_hex           = "#f8f8ff" 
	Gold_hex                 = "#ffd700" 
	Goldenrod_hex            = "#daa520" 
	Gray_hex                 = "#808080" 
	Green_hex                = "#008000" 
	Greenyellow_hex          = "#adff2f" 
	Grey_hex                 = "#808080" 
	Honeydew_hex             = "#f0fff0" 
	Hotpink_hex              = "#ff69b4" 
	Indianred_hex            = "#cd5c5c" 
	Indigo_hex               = "#4b0082" 
	Ivory_hex                = "#fffff0" 
	Khaki_hex                = "#f0e68c" 
	Lavender_hex             = "#e6e6fa" 
	Lavenderblush_hex        = "#fff0f5" 
	Lawngreen_hex            = "#7cfc00" 
	Lemonchiffon_hex         = "#fffacd" 
	Lightblue_hex            = "#add8e6" 
	Lightcoral_hex           = "#f08080" 
	Lightcyan_hex            = "#e0ffff" 
	Lightgoldenrodyellow_hex = "#fafad2" 
	Lightgray_hex            = "#d3d3d3" 
	Lightgreen_hex           = "#90ee90" 
	Lightgrey_hex            = "#d3d3d3" 
	Lightpink_hex            = "#ffb6c1" 
	Lightsalmon_hex          = "#ffa07a" 
	Lightseagreen_hex        = "#20b2aa" 
	Lightskyblue_hex         = "#87cefa" 
	Lightslategray_hex       = "#778899" 
	Lightslategrey_hex       = "#778899" 
	Lightsteelblue_hex       = "#b0c4de" 
	Lightyellow_hex          = "#ffffe0" 
	Lime_hex                 = "#00ff00" 
	Limegreen_hex            = "#32cd32" 
	Linen_hex                = "#faf0e6" 
	Magenta_hex              = "#ff00ff" 
	Maroon_hex               = "#800000" 
	Mediumaquamarine_hex     = "#66cdaa" 
	Mediumblue_hex           = "#0000cd" 
	Mediumorchid_hex         = "#ba55d3" 
	Mediumpurple_hex         = "#9370db" 
	Mediumseagreen_hex       = "#3cb371" 
	Mediumslateblue_hex      = "#7b68ee" 
	Mediumspringgreen_hex    = "#00fa9a" 
	Mediumturquoise_hex      = "#48d1cc" 
	Mediumvioletred_hex      = "#c71585" 
	Midnightblue_hex         = "#191970" 
	Mintcream_hex            = "#f5fffa" 
	Mistyrose_hex            = "#ffe4e1" 
	Moccasin_hex             = "#ffe4b5" 
	Navajowhite_hex          = "#ffdead" 
	Navy_hex                 = "#000080" 
	Oldlace_hex              = "#fdf5e6" 
	Olive_hex                = "#808000" 
	Olivedrab_hex            = "#6b8e23" 
	Orange_hex               = "#ffa500" 
	Orangered_hex            = "#ff4500" 
	Orchid_hex               = "#da70d6" 
	Palegoldenrod_hex        = "#eee8aa" 
	Palegreen_hex            = "#98fb98" 
	Paleturquoise_hex        = "#afeeee" 
	Palevioletred_hex        = "#db7093" 
	Papayawhip_hex           = "#ffefd5" 
	Peachpuff_hex            = "#ffdab9" 
	Peru_hex                 = "#cd853f" 
	Pink_hex                 = "#ffc0cb" 
	Plum_hex                 = "#dda0dd" 
	Powderblue_hex           = "#b0e0e6" 
	Purple_hex               = "#800080" 
	Red_hex                  = "#ff0000" 
	Rosybrown_hex            = "#bc8f8f" 
	Royalblue_hex            = "#4169e1" 
	Saddlebrown_hex          = "#8b4513" 
	Salmon_hex               = "#fa8072" 
	Sandybrown_hex           = "#f4a460" 
	Seagreen_hex             = "#2e8b57" 
	Seashell_hex             = "#fff5ee" 
	Sienna_hex               = "#a0522d" 
	Silver_hex               = "#c0c0c0" 
	Skyblue_hex              = "#87ceeb" 
	Slateblue_hex            = "#6a5acd" 
	Slategray_hex            = "#708090" 
	Slategrey_hex            = "#708090" 
	Snow_hex                 = "#fffafa" 
	Springgreen_hex          = "#00ff7f" 
	Steelblue_hex            = "#4682b4" 
	Tan_hex                  = "#d2b48c" 
	Teal_hex                 = "#008080" 
	Thistle_hex              = "#d8bfd8" 
	Tomato_hex               = "#ff6347" 
	Turquoise_hex            = "#40e0d0" 
	Violet_hex               = "#ee82ee" 
	Wheat_hex                = "#f5deb3" 
	White_hex                = "#ffffff" 
	Whitesmoke_hex           = "#f5f5f5" 
	Yellow_hex               = "#ffff00" 
	Yellowgreen_hex          = "#9acd32" 
)

var colores_hex = []string{ Aliceblue_hex, Antiquewhite_hex, Aqua_hex, Aquamarine_hex, Azure_hex, Beige_hex, Bisque_hex, Black_hex, Blanchedalmond_hex, Blue_hex, Blueviolet_hex, Brown_hex, Burlywood_hex, Cadetblue_hex, Chartreuse_hex, Chocolate_hex, Coral_hex, Cornflowerblue_hex, Cornsilk_hex, Crimson_hex, Cyan_hex, Darkblue_hex, Darkcyan_hex, Darkgoldenrod_hex, Darkgray_hex, Darkgreen_hex, Darkgrey_hex, Darkkhaki_hex, Darkmagenta_hex, Darkolivegreen_hex, Darkorange_hex, Darkorchid_hex, Darkred_hex, Darksalmon_hex, Darkseagreen_hex, Darkslateblue_hex, Darkslategray_hex, Darkslategrey_hex, Darkturquoise_hex, Darkviolet_hex, Deeppink_hex, Deepskyblue_hex, Dimgray_hex, Dimgrey_hex, Dodgerblue_hex, Firebrick_hex, Floralwhite_hex, Forestgreen_hex, Fuchsia_hex, Gainsboro_hex, Ghostwhite_hex, Gold_hex, Goldenrod_hex, Gray_hex, Green_hex, Greenyellow_hex, Grey_hex, Honeydew_hex, Hotpink_hex, Indianred_hex, Indigo_hex, Ivory_hex, Khaki_hex, Lavender_hex, Lavenderblush_hex, Lawngreen_hex, Lemonchiffon_hex, Lightblue_hex, Lightcoral_hex, Lightcyan_hex, Lightgoldenrodyellow_hex, Lightgray_hex, Lightgreen_hex, Lightgrey_hex, Lightpink_hex, Lightsalmon_hex, Lightseagreen_hex, Lightskyblue_hex, Lightslategray_hex, Lightslategrey_hex, Lightsteelblue_hex, Lightyellow_hex, Lime_hex, Limegreen_hex, Linen_hex, Magenta_hex, Maroon_hex, Mediumaquamarine_hex, Mediumblue_hex, Mediumorchid_hex, Mediumpurple_hex, Mediumseagreen_hex, Mediumslateblue_hex, Mediumspringgreen_hex, Mediumturquoise_hex, Mediumvioletred_hex, Midnightblue_hex, Mintcream_hex, Mistyrose_hex, Moccasin_hex, Navajowhite_hex, Navy_hex, Oldlace_hex, Olive_hex, Olivedrab_hex, Orange_hex, Orangered_hex, Orchid_hex, Palegoldenrod_hex, Palegreen_hex, Paleturquoise_hex, Palevioletred_hex, Papayawhip_hex, Peachpuff_hex, Peru_hex, Pink_hex, Plum_hex, Powderblue_hex, Purple_hex, Red_hex, Rosybrown_hex, Royalblue_hex, Saddlebrown_hex, Salmon_hex, Sandybrown_hex, Seagreen_hex, Seashell_hex, Sienna_hex, Silver_hex, Skyblue_hex, Slateblue_hex, Slategray_hex, Slategrey_hex, Snow_hex, Springgreen_hex, Steelblue_hex, Tan_hex, Teal_hex, Thistle_hex, Tomato_hex, Turquoise_hex, Violet_hex, Wheat_hex, White_hex, Whitesmoke_hex, Yellow_hex, Yellowgreen_hex }

var colores = []color.RGBA{ Aliceblue, Antiquewhite, Aqua, Aquamarine, Azure, Beige, Bisque, Black, Blanchedalmond, Blue, Blueviolet, Brown, Burlywood, Cadetblue, Chartreuse, Chocolate, Coral, Cornflowerblue, Cornsilk, Crimson, Cyan, Darkblue, Darkcyan, Darkgoldenrod, Darkgray, Darkgreen, Darkgrey, Darkkhaki, Darkmagenta, Darkolivegreen, Darkorange, Darkorchid, Darkred, Darksalmon, Darkseagreen, Darkslateblue, Darkslategray, Darkslategrey, Darkturquoise, Darkviolet, Deeppink, Deepskyblue, Dimgray, Dimgrey, Dodgerblue, Firebrick, Floralwhite, Forestgreen, Fuchsia, Gainsboro, Ghostwhite, Gold, Goldenrod, Gray, Green, Greenyellow, Grey, Honeydew, Hotpink, Indianred, Indigo, Ivory, Khaki, Lavender, Lavenderblush, Lawngreen, Lemonchiffon, Lightblue, Lightcoral, Lightcyan, Lightgoldenrodyellow, Lightgray, Lightgreen, Lightgrey, Lightpink, Lightsalmon, Lightseagreen, Lightskyblue, Lightslategray, Lightslategrey, Lightsteelblue, Lightyellow, Lime, Limegreen, Linen, Magenta, Maroon, Mediumaquamarine, Mediumblue, Mediumorchid, Mediumpurple, Mediumseagreen, Mediumslateblue, Mediumspringgreen, Mediumturquoise, Mediumvioletred, Midnightblue, Mintcream, Mistyrose, Moccasin, Navajowhite, Navy, Oldlace, Olive, Olivedrab, Orange, Orangered, Orchid, Palegoldenrod, Palegreen, Paleturquoise, Palevioletred, Papayawhip, Peachpuff, Peru, Pink, Plum, Powderblue, Purple, Red, Rosybrown, Royalblue, Saddlebrown, Salmon, Sandybrown, Seagreen, Seashell, Sienna, Silver, Skyblue, Slateblue, Slategray, Slategrey, Snow, Springgreen, Steelblue, Tan, Teal, Thistle, Tomato, Turquoise, Violet, Wheat, White, Whitesmoke, Yellow, Yellowgreen }


func Perfil(Nombre string) color.RGBA {
	if len(Nombre) > 0 {
		ascii := int(Nombre[0])
        if ascii < 0 && ascii>146 {
        	return White
        } 
		return colores[ascii]
	} else {
		return White
	}
}

func Perfilhex(Nombre string) string {
	if len(Nombre) > 0 {
		ascii := int(Nombre[0])
        if ascii < 0 && ascii>146 {
        	return White_hex
        } 
		return colores_hex[ascii]
	} else {
		return White_hex
	}
}
