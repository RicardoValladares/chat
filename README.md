# Chat

Este sencillo chat se construyó con la librería Fyne y usando el lenguaje de programación Go para permitir la compatibilidad con los sistemas operativos: FreeBSD, Linux, MacOS, Windows y Android.

También puedes usar este programa sin interfaz gráfica usando el argumento del nombre de usuario que quieres usar en el chat.


<p align="center">
  <img alt="Light" src="https://gitlab.com/RicardoValladares/chat/-/raw/main/capturas/consola.png" width="40%">
  &nbsp;
  <img alt="Light" src="https://gitlab.com/RicardoValladares/chat/-/raw/main/capturas/escritorio.png" width="25%">
  &nbsp;
  <img alt="Dark" src="https://gitlab.com/RicardoValladares/chat/-/raw/main/capturas/mobil.png" width="20%">
</p>


#### Instalar desde el compilador Go:

```bash
go install gitlab.com/RicardoValladares/chat@latest
```

#### Ejecutar con interfaz gráfica:

```bash
chat
```

#### Ejecutar sin interfaz gráfica, especificando el nombre de usuario:

```bash
chat Usuario
```

Este chat esta publico en interneta gracias a: https://github.com/RicardoValladares/PHP/tree/master/5_chat y http://ravr.webcindario.com/5_chat/
