package main

import (
	"gitlab.com/RicardoValladares/chat/colores"
	"gitlab.com/RicardoValladares/chat/chat"
	"gitlab.com/RicardoValladares/chat/participante"
	"fmt"
	"strings"
	"os"
	"time"
	"github.com/charmbracelet/bubbles/textarea"
	"github.com/charmbracelet/bubbles/viewport"
	"github.com/charmbracelet/lipgloss"
	tea "github.com/charmbracelet/bubbletea"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/widget"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/data/validation"    
	"fyne.io/fyne/v2/dialog"
)

var (
	hay bool	
	mensajes [][]string
	Chat = new(chat.Sala)
	usuario string
)

type (
	errMsg error
	tickMsg time.Time
)

type model struct {
	viewport    viewport.Model
	messages    []string
	textarea    textarea.Model
	senderStyle lipgloss.Style
	err         error
}


func main() {
	if len(os.Args) == 2 {
		usuario = os.Args[1]
		ta := textarea.New()
		ta.Placeholder = "Escribe un mensaje..."
		ta.Focus()
		ta.Prompt = usuario+": "
		ta.CharLimit = 100
		ta.SetWidth(60)
		ta.SetHeight(1)
		ta.FocusedStyle.CursorLine = lipgloss.NewStyle()
		ta.ShowLineNumbers = false
		vp := viewport.New(60, 15)
		vp.SetContent("Bienvenido a la sala de chat! \nDigita un mensaje y pulsa Enter para enviar.")
		ta.KeyMap.InsertNewline.SetEnabled(false)
		modelado := model{
			textarea:    ta,
			messages:    []string{},
			viewport:    vp,
			senderStyle: lipgloss.NewStyle().Foreground(lipgloss.Color("5")),
			err:         nil,
		}
		p := tea.NewProgram(modelado)
		if _, err := p.Run(); err != nil {
			fmt.Println(err)
		}
	} else {
		var yo string
		var entrymsj = widget.NewEntry()
		var Participaciones = binding.NewUntypedList()
		var list *widget.List
		var Notificarme = false
		a := app.NewWithID("gitlab.com.ricardovalladares.chat")
		a.Settings().SetTheme(theme.DarkTheme())
		w := a.NewWindow("chat")
		w.Resize(fyne.Size{Width: 350, Height: 500})
		username := widget.NewEntry()
		username.Validator = validation.NewRegexp(`^[A-Za-z0-9_-]+$`, "nombre de usuario solo puede contener letras, numeros, '_', y '-'")
		items := []*widget.FormItem{
			widget.NewFormItem("Usuario: ", username),			
			widget.NewFormItem("Notificarme: ", widget.NewCheck("", func(checked bool) {
				Notificarme = checked
			})),
		}
		dialog.ShowForm("Inicio de session", "Iniciar", "Cancel", items, func(b bool) {
			if !b {
				os.Exit(3)
				return
			}
			yo = username.Text
		},w)
		list = widget.NewList(
			func() int {
					return Participaciones.Length()
			}, 
			func() fyne.CanvasObject {
				label := widget.NewLabel("Mensaje")
				label.Wrapping = fyne.TextWrapBreak
				return container.NewBorder(canvas.NewText("Usuario", colores.Blue), nil, nil, nil, label)
			},
			func(id widget.ListItemID, co fyne.CanvasObject) {
				ctr, _ := co.(*fyne.Container)
				val, _ := Participaciones.GetValue(id)
				user := (val).(participante.Paticipacion).Usuario
				cuser := ctr.Objects[1].(*canvas.Text)
				if len(user) > 10 {
					cuser.Text = user[:10]
				} else {
					cuser.Text = user
				}
				cuser.Color =  colores.Perfil(user)
				cuser.Refresh()
				lmessage := ctr.Objects[0].(*widget.Label)
				message := (val).(participante.Paticipacion).Mensaje
				if len(message) > 100 {
					lmessage.SetText(message[:100])
				} else {
					lmessage.SetText(message)
				}
				list.SetItemHeight(id, 80)
			},
		)
		list.OnSelected = func(id widget.ListItemID) {
			listitemid, _ := Participaciones.GetValue(id)
			w.Clipboard().SetContent( (listitemid).(participante.Paticipacion).Mensaje )
		}
		w.SetContent(container.NewBorder(container.NewBorder( nil, nil, nil, widget.NewButtonWithIcon("", theme.MailSendIcon(), func() { 
			message := entrymsj.Text
			if len(entrymsj.Text) > 100 {
				message = entrymsj.Text[:100]
			}
			
			smensajes, shay := Chat.Interaccion(yo, message)
			if shay {
				for i:=0; i<len(smensajes); i++ {
					if len(smensajes[i])==2 {
						size := Participaciones.Length()
						if size>0 {
							Participaciones.Append(participante.Paticipar("",""))
							for i:=size; i>=0; i--{
								val, _ := Participaciones.GetValue(i)
								Participaciones.SetValue(i+1, val)
							}
							Participaciones.SetValue(0, participante.Paticipar(smensajes[i][0], smensajes[i][1])) 
							list.Refresh()
						} else {
							Participaciones.Append(participante.Paticipar(smensajes[i][0], smensajes[i][1]))
							list.Refresh()
						}
					}
				}
				entrymsj.Text = ""	
				entrymsj.Refresh()
			}
		}),entrymsj), nil, nil, nil, list))
		
		go func() {
			for {
				notificaciones := ""
				nmensajes, nhay := Chat.Interaccion("","")
				if nhay {
					for i:=0; i<len(nmensajes); i++ {
						if len(nmensajes[i])==2 {				
							size := Participaciones.Length()
							if size>0 {
								Participaciones.Append(participante.Paticipar("",""))
								for i:=size; i>=0; i--{
									val, _ := Participaciones.GetValue(i)
									Participaciones.SetValue(i+1, val)
								}
								notificaciones=notificaciones+nmensajes[i][0]+nmensajes[i][1]+"\n\n"
								Participaciones.SetValue(0, participante.Paticipar(nmensajes[i][0], nmensajes[i][1])) 
								list.Refresh()
							} else {
								notificaciones=notificaciones+nmensajes[i][0]+nmensajes[i][1]+"\n\n"
								Participaciones.Append(participante.Paticipar(nmensajes[i][0], nmensajes[i][1]))
								list.Refresh()
							}
						}
					}	
					if Notificarme {
						fyne.CurrentApp().SendNotification(&fyne.Notification{Title: "Chat", Content: notificaciones, })
					}
				}				
				time.Sleep(1 * time.Second)
			}
		}()
			
		w.ShowAndRun()
	}
}


func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var (
		tiCmd tea.Cmd
		vpCmd tea.Cmd
	)
	m.textarea, tiCmd = m.textarea.Update(msg)
	m.viewport, vpCmd = m.viewport.Update(msg)
	switch msg := msg.(type) {
		case tea.KeyMsg:

			switch msg.Type {
				case tea.KeyRunes:
		            m.viewport.GotoBottom()
		            return m, tea.Batch(tiCmd, vpCmd)
				case tea.KeyCtrlC, tea.KeyEsc:
					fmt.Println(m.textarea.Value())
					return m, tea.Quit
				case tea.KeyEnter:
						smensajes, shay := Chat.Interaccion(usuario,m.textarea.Value())
						if shay {
							for i:=0; i<len(smensajes); i++ {
								if len(smensajes[i])==2 {
									m.senderStyle = lipgloss.NewStyle().Foreground( lipgloss.Color(colores.Perfilhex( smensajes[i][0] ) ) )
									m.messages = append(m.messages, m.senderStyle.Render(smensajes[i][0])+smensajes[i][1])
									m.viewport.SetContent(strings.Join(m.messages, "\n"))
								}
							}	
							m.viewport.GotoBottom()
						}
						m.textarea.Reset()
						return m, tea.Batch(tiCmd, vpCmd)
			}

		case tickMsg:
			if hay {
				for i:=0; i<len(mensajes); i++ {
					if len(mensajes[i])==2 {
						m.senderStyle = lipgloss.NewStyle().Foreground( lipgloss.Color(colores.Perfilhex( mensajes[i][0] ) ) )
						m.messages = append(m.messages, m.senderStyle.Render(mensajes[i][0])+mensajes[i][1])
						m.viewport.SetContent(strings.Join(m.messages, "\n"))
					}
				}	
				m.viewport.GotoBottom()
			}
			return m, tickCmd()
		case errMsg:
			m.err = msg
			return m, nil
	}
	return m, tea.Batch(tiCmd, vpCmd)
}

func (m model) Init() tea.Cmd {
	return tickCmd()
	//return textarea.Blink
}

func (m model) View() string {
	return fmt.Sprintf("%s\n\n%s", m.viewport.View(), m.textarea.View()) + "\n\n"
}

func tickCmd() tea.Cmd {
	return tea.Tick(time.Second*1, func(t time.Time) tea.Msg { 
		mensajes, hay = Chat.Interaccion("", "") 
		return tickMsg(t) 
	})
}

