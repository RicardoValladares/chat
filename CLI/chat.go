package main

import (
	"gitlab.com/RicardoValladares/chat/colores"
	"gitlab.com/RicardoValladares/chat/chat"
	"fmt"
	"strings"
	"os"
	"time"
	"github.com/charmbracelet/bubbles/textarea"
	"github.com/charmbracelet/bubbles/viewport"
	"github.com/charmbracelet/lipgloss"
	tea "github.com/charmbracelet/bubbletea"
	"os/exec"
)

var (
	hay bool	
	mensajes [][]string
	Chat = new(chat.Sala)
	usuario string
)

type (
	errMsg error
	tickMsg time.Time
)

type model struct {
	viewport    viewport.Model
	messages    []string
	textarea    textarea.Model
	senderStyle lipgloss.Style
	err         error
}


func main() {
	if len(os.Args) == 2 {
		usuario = os.Args[1]
		ta := textarea.New()
		ta.Placeholder = "Escribe un mensaje..."
		ta.Focus()
		ta.Prompt = usuario+": "
		ta.CharLimit = 100
		ta.SetWidth(60)
		ta.SetHeight(1)
		ta.FocusedStyle.CursorLine = lipgloss.NewStyle()
		ta.ShowLineNumbers = false
		vp := viewport.New(60, 15)
		vp.SetContent("Bienvenido a la sala de chat! \nDigita un mensaje y pulsa Enter para enviar.")
		ta.KeyMap.InsertNewline.SetEnabled(false)
		modelado := model{
			textarea:    ta,
			messages:    []string{},
			viewport:    vp,
			senderStyle: lipgloss.NewStyle().Foreground(lipgloss.Color("5")),
			err:         nil,
		}
		p := tea.NewProgram(modelado)
		if _, err := p.Run(); err != nil {
			fmt.Println(err)
		}
	} else {
		cmd := exec.Command("am", "start", "--user", "0", "-n", "gitlab.com.ricardovalladares.chat/org.golang.app.GoNativeActivity")
		err := cmd.Run()
		if err != nil {
			if exec.Command("termux-open-url", "https://gitlab.com/-/project/58359248/uploads/8916d1317a76632264c3e6fc417751ca/chat.apk").Run() != nil {
				fmt.Println("No se puede ejecutar interfaz grafica, para usar la consola pon un nombre de usuario")
			}
		}
	}
}


func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var (
		tiCmd tea.Cmd
		vpCmd tea.Cmd
	)
	m.textarea, tiCmd = m.textarea.Update(msg)
	m.viewport, vpCmd = m.viewport.Update(msg)
	switch msg := msg.(type) {
		case tea.KeyMsg:

			switch msg.Type {
				case tea.KeyRunes:
					m.viewport.GotoBottom()
					return m, tea.Batch(tiCmd, vpCmd)
				case tea.KeyCtrlC, tea.KeyEsc:
					fmt.Println(m.textarea.Value())
					return m, tea.Quit
				case tea.KeyEnter:
					smensajes, shay := Chat.Interaccion(usuario,m.textarea.Value())
					if shay {
						for i:=0; i<len(smensajes); i++ {
							if len(smensajes[i])==2 {
								m.senderStyle = lipgloss.NewStyle().Foreground( lipgloss.Color(colores.Perfilhex( smensajes[i][0] ) ) )
								m.messages = append(m.messages, m.senderStyle.Render(smensajes[i][0])+smensajes[i][1])
								m.viewport.SetContent(strings.Join(m.messages, "\n"))
							}
						}	
						m.viewport.GotoBottom()
					}
					m.textarea.Reset()
					return m, tea.Batch(tiCmd, vpCmd)
			}

		case tickMsg:
			if hay {
				for i:=0; i<len(mensajes); i++ {
					if len(mensajes[i])==2 {
						m.senderStyle = lipgloss.NewStyle().Foreground( lipgloss.Color(colores.Perfilhex( mensajes[i][0] ) ) )
						m.messages = append(m.messages, m.senderStyle.Render(mensajes[i][0])+mensajes[i][1])
						m.viewport.SetContent(strings.Join(m.messages, "\n"))
					}
				}	
				m.viewport.GotoBottom()
			}
			return m, tickCmd()
		case errMsg:
			m.err = msg
			return m, nil
	}
	return m, tea.Batch(tiCmd, vpCmd)
}

func (m model) Init() tea.Cmd {
	return tickCmd()
	//return textarea.Blink
}

func (m model) View() string {
	return fmt.Sprintf("%s\n\n%s", m.viewport.View(), m.textarea.View()) + "\n\n"
}

func tickCmd() tea.Cmd {
	return tea.Tick(time.Second*1, func(t time.Time) tea.Msg { 
		mensajes, hay = Chat.Interaccion("", "") 
		return tickMsg(t) 
	})
}
